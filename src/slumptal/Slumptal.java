//******************************************************************
// Programmerare: Robin Will�n wilrob-2
// Datum: 2016-03-04
// Senast uppdaterad: 2016-03-05 Robin Will�n
// Beskrivning: This Applications prints random numbers in the inteval 0 - 999 and the process the numbers if they are even or odd.
// Version: 0.2 add {}round all the loops
//******************************************************************

package slumptal;

import java.util.Scanner;
import java.lang.Math;

public class Slumptal {

	public static void main(String[] args) {
	//Declaring variables	
	int number = 0;			//Input from user how many numbers in the interval 0 - 999 the user want.
	int even = 0;			//The variable counting the even numbers.
	int odd = 0;			//The variable counting the uneven numbers.
	int counter = 0;		//The variable counting every time an number get in to the smaller evenArray or oddArray.
	
	Scanner in = new Scanner(System.in); //Scanner for user input.
		
	//User input
	System.out.print("Hur m�nga slumtal i intervallet 0 - 999 �nskas?");
	number = in.nextInt();
	
	//Processing random numbers in to array.
	int[] numbers = new int[number]; //Declaring the array.
	for(int i = 0; i < number ; i++)
	{
		numbers[i] = (int)(Math.random() * 1000);
	}
	
	System.out.println("\nH�r �r de slumpade talen:"); //User output
	
	//Printing the random numbers in the array.
	for(int i = 0; i < number ; i++)
	{
		System.out.print(numbers[i] + " ");
	}
	
	//Processing array even or odd
	for(int i = 0 ; i < number ; i++)
	{
		if( numbers[i] % 2 == 0 )
		{
			even++;
		}
		else
		{
			odd++;
		}
	}
	//Processing even random numbers into array.
	int[] evenArray = new int[even];
	
	for(int i = 0 ; i < number ; i++)
	{
		if( numbers[i] % 2 == 0 )
		{
			evenArray[counter] = numbers[i];
			counter++;
		}
	}
	
	//Printing the even random numbers.
	System.out.println( "\n\nDessa " + even + " tal �r j�mna:");

	for(int i = 0; i < counter ; i++)
	{
		System.out.print(evenArray[i] + " ");
	}
	
	//Processing odd random numbers into array.
	int[] oddArray = new int[odd];
	counter = 0; //reset the counter
	
	for(int i = 0 ; i < number ; i++)
	{
		if( numbers[i] % 2 == 0 ){}
		else
		{
			oddArray[counter] = numbers[i];
			counter++;
		}
	}
	
	//Printing the odd random numbers.
	System.out.println("\n\nDessa " + odd + " tal �r udda:");

	for(int i = 0; i < counter ; i++)
	{
		System.out.print(oddArray[i] + " ");
	}
	
	in.close();	//Closing the scanner.
	}

}
